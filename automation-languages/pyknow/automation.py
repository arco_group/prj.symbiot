#!/usr/bin/python3
from pyknow import KnowledgeEngine, Fact, Rule, AND, AS, MATCH
import pprint


class Switch(Fact):
    pass

class Event(Fact):
    pass

class Automation(KnowledgeEngine):
    @Rule(
        AND(
            AS.switch << Switch(identifier='switch-01'),
            AS.event  << Event(identifier='presence-01')
        )
    )
    def turn_on_when_presence(self, switch, event):
        self.retract(event)
        self.modify(switch, status=True)

    @Rule(Switch(identifier=MATCH.identifier, status=True))
    def switch_on(self, identifier):
        print("Switch {} is now ON".format(identifier))



if __name__ == '__main__':
    engine = Automation()
    engine.reset()
    engine.declare(Switch(identifier='switch-01', status=False))
    engine.declare(Event(identifier='presence-01'))

    pp = pprint.PrettyPrinter(indent=4)

    # introspection
    print("facts:")
    print(engine.facts)
    print("\nfacts[1] class:")
    print(engine.facts[1].__class__)
    print("\nfacts[1] fields:")
    print(engine.facts[1].as_dict())
    print("\nrules:")
    pp.pprint(engine.get_rules())

    print("\nrule[0]:")
    rule = engine.get_rules()[0]
    print(rule)
    print("\nrule[0] conditions:")
    print(rule[0].as_dict())


    # run
    print("\nExecution:")
    print(engine.facts)
    engine.run()
    print("---")
    print(engine.facts)
