#!/usr/bin/python

'''
- https://www.csie.ntu.edu.tw/~sylee/courses/clips/bpg/top.html
- http://pyclips.sourceforge.net/manual/pyclips.html
- https://www.csie.ntu.edu.tw/~sylee/courses/clips/toc.htm
'''

import clips

def notify(source, value):
    print("Python notification {} -> {}".format(source, value))

clips.RegisterPythonFunction(notify)
clips.Load('automation.clp')

clips.Reset()
clips.PrintFacts()


# ;(python-call notify switch-01 ?status)

