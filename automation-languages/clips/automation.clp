(deftemplate Switch
    (field identifier)
    (field status)
)

(deftemplate Event
    (field identifier)
)

(deffacts initial
    (Switch
        (identifier switch-01)
        (status false)
    )
)

(defrule turn-on-when-presence
    ?event <- (Event (identifier presence-01))
    ?device-status <- (Switch (identifier switch-01))
    =>
    (retract ?event)
    (modify ?device-status (status true))
)

(deffunction presence ()
     (assert (Event (identifier presence-01)))
     (run)
)

; (load templates.clp)