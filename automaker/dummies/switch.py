#!usr/bin/python3

import Ice
import sys

Ice.loadSlice("/usr/share/slice/st/st.ice")
import st


class SwitchI(st.IBool, st.Linkable):
    def __init__(self):
        self.status = False
        self.observer = None

    def set(self, new_value, sourceAddr=None, current=None):
        self.status = new_value
        print("The switch is", "on" if self.status else "off")
        
        if self.observer is not None:
            self.observer.set(self.status)

    def linkto(self, observer, current=None):
        ic = current.adapter.getCommunicator()
        prx = ic.stringToProxy(observer)
        self.observer = st.IBoolPrx.uncheckedCast(prx)
        print("new observer set: '{}'".format(str(prx)))


class SwitchServer(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("Adapter")
        adapter.activate()
        
        switch = SwitchI()

        proxy = adapter.add(switch, ic.stringToIdentity("Switch"))
        print(proxy)

        self.shutdownOnInterrupt()
        self.communicator().waitForShutdown()

        return 0


if __name__ == '__main__':
    SwitchServer().main(sys.argv)
