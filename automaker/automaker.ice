// -*- mode: c++; coding: utf-8 -*-

module Automaker{

    struct Copy {
        string deviceId;
        string field;
    };

    // struct ConditionPredicate {
    //   string field;
    //   string value;
    // };
    sequence<string> ConditionPredicate;
    sequence<ConditionPredicate> ConditionList;
    dictionary<string, ConditionList> ConditionDict;

    interface Operator {
        void setEvent(string when);
        void setAction(string then);
        void setActionWithVar(Copy var);
        void setDelay(int delay);
        void setConditions(ConditionDict conditions);
        void commit();
    };
};
