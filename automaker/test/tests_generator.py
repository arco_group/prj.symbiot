#!/usr/bin/python3

import sys
import io
from unittest import TestCase
from argparse import Namespace

import Ice
from doublex import Spy, Stub, assert_that, called, never

from scn import \
    (Rule, Statement, Action, InvalidCondition, InvalidAction,
     Event, Template, Engine, CodeGenerator, InvalidSymbolTable,
     Provisioner, Check, var, Delay)


Switch = Template('Switch', id=str, status=bool)
DSwitch = Template("DSwitch", id=str, status1=bool, status2=bool)
Day = Template('Day', id=str, sunlight=bool)
Thermometer = Template('Thermometer', id=str, temperature=int)

class CodeGeneratorTest(TestCase):
    def test_create_generator_with_symbol_table(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')

        rule = Rule(
            Event(sw_01.when(status=True))
        )(
            sw_02.set(status=True)
        )

        CodeGenerator([rule], 'Example')

    def test_empty_symbol_table(self):
        with self.assertRaises(InvalidSymbolTable):
            CodeGenerator([], file_name=None)

    def test_check_bad_rule_in_symbol_table(self):
        with self.assertRaises(InvalidSymbolTable):
            CodeGenerator(["bad rule"], file_name=None)

    def test_code_generated(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')

        rule = Rule(
            Event(sw_01.when(status=True))
        )(
            sw_02.set(status=True)
        )

        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='True', met={}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''

        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())

    def test_code_generated_with_condition(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        day_01 = Day(id='day-01')

        rule = Rule(
            Event(sw_01.when(status=True)),
            day_01.when(sunlight=False)
        )(
            sw_02.set(status=True)
        )

        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='True', met={'day-01': [('sunlight', 'False')]}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''

        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())

    def test_code_generated_with_threshold(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        temp_01 = Thermometer(id='temp-01')

        rule = Rule(
            Event(sw_01.when(status=True)),
            temp_01.when(temperature='<20')
        )(
            sw_02.set(status=True)
        )

        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='True', met={'temp-01': [('temperature', '<20')]}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''

        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())

    def test_with_threshold_and_one_condition(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        sw_03 = Switch(id='sw-03')
        temp_01 = Thermometer(id='temp-01')

        rule = Rule(
            Event(sw_01.when(status=True)),
            temp_01.when(temperature='<20'),
            sw_03.when(status=False)
        )(
            sw_02.set(status=True)
        )

        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='True', met={'temp-01': [('temperature', '<20')], 'sw-03': [('status', 'False')]}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''

        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertCountEqual(expected, fd.read())

    def test_event_with_threshold(self):
        sw_01 = Switch(id='sw-01')
        th_01 = Thermometer(id='th-01')
    
        rule = Rule(
            Event(th_01.when(temperature='<20')),
        )(
            sw_01.set(status=True)
        )        
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('FloatOperatorTemplate', 'FloatExampleRule1Op1')
configureOperator(op, when='<20', met={}, then='True', delay=0)
linkServices(str(op), source='th-01', sink='sw-01')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())      

    def test_event_with_wildcard_any(self):
        temp_01 = Thermometer(id='temp-01')
        sw_01 = Switch(id='sw-01')

        rule = Rule(
            Event(temp_01.any())
        )(
            sw_01.set(status=True)
        )     
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='', met={}, then='True', delay=0)
linkServices(str(op), source='temp-01', sink='sw-01')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())

    def test_event_with_var(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')

        rule = Rule(
            Event(sw_02.when(status=True))
        )(
            sw_01.set(status=sw_02.status)
        )     
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperatorWithVar(op, when='True', met={}, then=Copy('sw-02', 'status'), delay=0)
linkServices(str(op), source='sw-02', sink='sw-01')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())      

    def test_event_with_var_and_any(self):
        temp_01 = Thermometer(id='temp-01')
        sw_01 = Switch(id='sw-01')
        var.clean()

        rule = Rule(
            Event(temp_01.any())
        )(
            sw_01.set(status=temp_01.status)
        )  
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperatorWithVar(op, when='', met={}, then=Copy('temp-01', 'status'), delay=0)
linkServices(str(op), source='temp-01', sink='sw-01')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read()) 

    def test_rule_with_two_actions(self):
        
        temp_01 = Thermometer(id='temp-01')
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        var.clean()

        rule = Rule(
            Event(temp_01.any())
        )(
            sw_01.set(status=False),
            sw_02.set(status=True)
        )  
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='', met={}, then='False', delay=0)
linkServices(str(op), source='temp-01', sink='sw-01')


op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op2')
configureOperator(op, when='', met={}, then='True', delay=0)
linkServices(str(op), source='temp-01', sink='sw-02')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read()) 

    def test_rule_with_delay(self):
        temp_01 = Thermometer(id='temp-01')
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        var.clean()

        rule = Rule(
            Event(temp_01.any())
        )(
            sw_01.set(status=False),
            Delay(30),
            sw_02.set(status=True)
        )  
            
        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='', met={}, then='False', delay=0)
linkServices(str(op), source='temp-01', sink='sw-01')


op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op2')
configureOperator(op, when='', met={}, then='True', delay=30)
linkServices(str(op), source='temp-01', sink='sw-02')
'''
        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertEquals(expected, fd.read())             

    def test_condition_with_two_fields(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        sw_03 = DSwitch(id='sw-03')
        temp_01 = Thermometer(id='temp-01')

        rule = Rule(
            Event(sw_01.when(status=True)),
            temp_01.when(temperature='<20'),
            sw_03.when(status1=False, status2=True)
        )(
            sw_02.set(status=True)
        )

        sut = CodeGenerator([rule], 'Example')
        sut.build()

        expected ='''
op = createOperator('BoolOperatorTemplate', 'BoolExampleRule1Op1')
configureOperator(op, when='True', met={'temp-01': [('temperature', '<20')], 'sw-03': [('status1', 'False'), ('status2', 'True')]}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''

        with io.StringIO() as fd:
            sut.write_to_fd(fd)
            fd.seek(0)
            self.assertCountEqual(expected, fd.read())

     


