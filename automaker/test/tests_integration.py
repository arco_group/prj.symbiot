import unittest
import hamcrest

from prego import context as ctx, Task, File, TestCase, exists, terminated
from prego.net import localhost, listen_port

IG_ADMIN = 'icegridadmin --Ice.Config=locator.config -u user -p pass -e '


class TestIntegration(TestCase):
    def clean(self):
        clean = Task()
        clean.command('killall icegridnode', expected=None)
        clean.command('rm -rf /tmp/db')
        clean.command('rm -rf fixtures/example.prv')

    def setUp(self):
        self.clean()

    def test_compile_only(self):
        cmd = './scn.py -c fixtures/example.scn --Ice.Config=fixtures/switch_scenary.config'

        scn = Task()
        scn.command(cmd)
        provisioner_file = File('fixtures/example.prv')

        scn.assert_that(provisioner_file, exists())
        scn.assert_that(provisioner_file.content, hamcrest.contains_string('''
op = createOperator('BoolOperatorTemplate', 'BoolexampleRule1Op1')
configureOperator(op, when='True', met={}, then='True', delay=0)
linkServices(str(op), source='sw-01', sink='sw-02')
'''))

    def test_provision_only(self):
        ctx.port = 4061
        ctx.cwd = "app-fixtures/src/switch_scenary/"
        compile_cmd = '../../../scn.py  -c ../../../fixtures/example.scn --Ice.Config=../../../fixtures/switch_scenary.config'
        provision_cmd = '../../../scn.py -p ../../../fixtures/example.prv --Ice.Config=../../../fixtures/switch_scenary.config'

        Task().command(compile_cmd)

        Task().command('mkdir -p /tmp/db/registry /tmp/db/node1')
        
        Task(desc='icegrid node', detach=True).\
            command('icegridnode --Ice.Config=node1.config')

        add_app = Task(desc='add-app')
        add_app.wait_that(localhost, listen_port(ctx.port))
        add_app.command(IG_ADMIN + '"application add app.xml"')

        Task(desc='provision task').command(provision_cmd)

        operator_file = File(
            '/tmp/db/node1/servers/BoolexampleRule1Op1/distrib/bool.out')
        Task().assert_that(operator_file.content, hamcrest.contains_string(
            "new observer set: 'sw-02"))


    def test_compile_and_provision_without_options(self):
        ctx.port = 4061
        ctx.cwd = "app-fixtures/src/switch_scenary/"
        cmd = '../../../scn.py  ../../../fixtures/example.scn --Ice.Config=../../../fixtures/switch_scenary.config'
        
        Task().command('mkdir -p /tmp/db/registry /tmp/db/node1')
        
        Task(desc='icegrid node', detach=True).\
            command('icegridnode --Ice.Config=node1.config')

        add_app = Task(desc='add-app')
        add_app.wait_that(localhost, listen_port(ctx.port))
        add_app.command(IG_ADMIN + '"application add app.xml"')

        Task().command(cmd)

        operator_file = File(
            '/tmp/db/node1/servers/BoolexampleRule1Op1/distrib/bool.out')
        Task().assert_that(operator_file.content, hamcrest.contains_string(
            "new observer set: 'sw-02"))

    def tearDown(self):
        self.clean()


        
