#!/usr/bin/python3

import io
import sys
from argparse import Namespace
from unittest import TestCase, skip

from doublex import Stub, Spy, Mock, assert_that, called, never, verify
import Ice
import scn
from scn import \
    (Rule, Statement, Action, InvalidCondition, InvalidAction,
     Event, Template, Engine, CodeGenerator, InvalidSymbolTable,
     Provisioner)


Switch = Template('Switch', id=str, status=bool)


class TestsProvisioner(TestCase):
    def setUp(self):
        self.operator = Stub()
        self.op_prx = Stub()

        with Mock() as self.factory:
            self.factory.make(
                'node1', 'Template', {'name': 'Server'}
                ).returns(self.op_prx)

        scn.Automaker = Stub()

        with Mock() as scn.Automaker.OperatorPrx:
            scn.Automaker.OperatorPrx.checkedCast(self.op_prx).returns(self.operator)

        self.wiring = Spy()

        self.property_server = Stub()

        self.outfile_descriptor = io.StringIO('''
op_str_proxy, op = createOperator('OperatorTemplate', 'BoolServer', delay=0)
configureOperator(op, when=True, met=[], then=True)
linkServices(op_str_proxy, source='sw-01', sink='sw-02')
''')

        with Stub() as self.properties:
            self.properties.getProperty('node').returns('node1')

        self.argv = ['--Ice.Config=bool-to-bool.config']

    def test_create_operator(self):
        resolver = Stub()
        
        with Ice.initialize() as ic:
            ic.getProperties = lambda:self.properties
            sut = Provisioner(self.factory, self.wiring, self.property_server, "placeholder.prv", ic, resolver)
            op = sut.createOperator('Template', 'Server')
            self.assertEquals(op, self.operator)

    def test_create_and_configure_operator(self):
        with Mock() as self.operator:
            self.operator.setEvent('True')
            self.operator.setAction('True')
            self.operator.setConditions([])
            self.operator.setDelay(30)
            self.operator.commit()

        resolver = Stub()

        with Ice.initialize() as ic:
            ic.getProperties = lambda:self.properties
            sut = Provisioner(self.factory, self.wiring, self.property_server, "placeholder.prv", ic, resolver)
            op = sut.createOperator('Template', 'Server')
            sut.configureOperator(op, True, [], True, delay=30)

    def test_create_and_configure_operator_with_var(self):
        with Mock() as self.operator:
            self.operator.setEvent('True')
            self.operator.setActionWithVar('True')
            self.operator.setConditions([])
            self.operator.setDelay(30)
            self.operator.commit()

        resolver = Stub()

        with Ice.initialize() as ic:
            ic.getProperties = lambda:self.properties
            sut = Provisioner(self.factory, self.wiring, self.property_server, "placeholder.prv", ic, resolver)
            op = sut.createOperator('Template', 'Server')
            sut.configureOperatorWithVar(op, True, [], True, delay=30)

    def test_link_two_objects(self):
        with Stub() as resolver:
            resolver.resolve('sw-01', self.property_server).returns(
                'sw-01 -t -e 1.1 @ SwitchServer1.SwitchAdapter')
            resolver.resolve('sw-02', self.property_server).returns(
                'sw-02 -t -e 1.1 @ SwitchServer2.SwitchAdapter')

        with Ice.initialize() as ic:
            ic.getProperties = lambda: self.properties
            sut = Provisioner(self.factory, self.wiring,
                              self.property_server, "placeholder.prv", ic, resolver)
            sut.linkServices('operator', 'sw-01', 'sw-02')

        assert_that(self.wiring.addObserver, called().with_args(
            'sw-01 -t -e 1.1 @ SwitchServer1.SwitchAdapter', 'operator'))
        assert_that(self.wiring.addObserver, called().with_args(
            'operator', 'sw-02 -t -e 1.1 @ SwitchServer2.SwitchAdapter'))

    # @skip("demonstrating skipping")
    # def test_link_two_sources_with_two_sinks(self):
    #     with Stub() as resolver:
    #         resolver.resolve('sw-01').returns('sw-01 -t -e 1.1 @ SwitchServer1.SwitchAdapter')
    #         resolver.resolve('sw-02').returns('sw-02 -t -e 1.1 @ SwitchServer2.SwitchAdapter')

    #     with Ice.initialize() as ic:
    #         ic.getProperties = lambda:self.properties
    #         sut = Provisioner(self.factory, self.wiring, "placeholder.prv", ic, resolver)

    #     sut.provide('Template', 'Server', 'True', ['None'], 'True',
    #                 ['sw-01', 'sw-02'], ['None'], ['sw-01', 'sw-02'])

    #     assert_that(self.wiring.addObserver, called().with_args(
    #         'sw-01 -t -e 1.1 @ SwitchServer1.SwitchAdapter', 'operator'))
    #     assert_that(self.wiring.addObserver, called().with_args(
    #         'sw-02 -t -e 1.1 @ SwitchServer2.SwitchAdapter', 'operator'))
    #     assert_that(self.wiring.addObserver, called().with_args(
    #         'operator', 'sw-01 -t -e 1.1 @ SwitchServer1.SwitchAdapter'))
    #     assert_that(self.wiring.addObserver, called().with_args(
    #         'operator', 'sw-02 -t -e 1.1 @ SwitchServer2.SwitchAdapter'))
