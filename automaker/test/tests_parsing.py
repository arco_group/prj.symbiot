#!/usr/bin/python3

import io

from unittest import TestCase
from argparse import Namespace

from doublex import Spy, Stub, assert_that, called, never
from scn import \
    (Rule, Statement, Action, InvalidCondition, InvalidAction,
     Event, Template, Engine, InvalidFieldType, Parser,
     SemanticAnalyzer, EventNotFound, MissingField,
     parse_args, Condition, TypeConflict, IllegalEvent, Check,
     InvalidCheck, var, InvalidFact, Delay, InvalidDelayTime,
     when)


Switch = Template('Switch', id=str, status=bool)
Sun = Template('Sun', id=str, sunlight=bool)
Thermometer = Template('Thermometer', id=str, temperature=int)


class RuleTests(TestCase):
    def test_empty_conditions(self):
        with self.assertRaises(EventNotFound):
            Rule()

    def test_empty_actions(self):
        sw_01 = Switch(id='sw_01')
        sut = Rule(Event(sw_01.any()))
        self.assertEquals(sut.actions, [])

    def test_one_condition(self):
        sw_01 = Switch(id='sw_01')
        statement = sw_01.any()
        sut = Rule(Event(statement))
        self.assertEquals(sut.conditions, [])
        self.assertEquals(sut.event, statement)

    def test_rule_check_condition_is_Statement(self):
        with self.assertRaises(InvalidCondition):
            Rule("bad statement")

    def test_rule_check_action_is_Action(self):
        sw_01 = Switch(id='sw_01')        
        with self.assertRaises(InvalidAction):
            Rule(Event(sw_01.any()))("bad action")

    def test_one_action(self):
        sw_01 = Switch(id='sw_01')
        action = sw_01.set(status=True)
        sut = Rule(Event(sw_01.any()))(action)
        self.assertEquals(sut.actions, [action])

    def test_define_type(self):
        sut = Switch(id='sw-01', status=True)
        self.assertEquals(sut['id'], 'sw-01')

    def test_rule_set_true_on_true(self):
        switch1 = Switch(id='sw-01', status=True)
        switch2 = Switch(id='sw-02', status=True)
        
        sut = Rule(
            Event(switch1.when(status=True)),
        )(
            switch2.set(status=True)
        )

        self.assertTrue(isinstance(sut.event, Event))
        self.assertEquals(sut.event.id, 'sw-01')
        self.assertEquals(sut.actions[0].id, 'sw-02')
        self.assertTrue(sut.event['status'])
        self.assertTrue(sut.actions[0]['status'])

    def test_rules_are_stored_in_Rule(self):
        sw_01 = Switch(id='sw_01')
        Engine.reset() 
        sut = Rule(Event((sw_01.any())))
        self.assertEquals(Engine.rules[0], sut)

    def test_type_without_identifier(self):
        with self.assertRaises(MissingField) as ex:
            Template('Switch', status=bool)
        self.assertEquals("id", str(ex.exception))

    def test_rule_with_event_and_condition(self):
        sw_01 = Switch(id='sw-01')
        sun = Sun(id='sun')
        sw_02 = Switch(id='sw-02')

        sut = Rule(
            Event(sw_01.when(status=True)),
            sun.when(sunlight=False)
        )(
            sw_02.set(status=True)
        )

        self.assertEquals(sut.event.id, 'sw-01')
        self.assertEquals(sut.conditions[0].id, 'sun')
        self.assertEquals(sut.actions[0].id, 'sw-02')
          
    def test_rule_without_event(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        with self.assertRaises(EventNotFound):
            Rule(
                sw_01.when(status=True)
            )(
                sw_02.set(status=True)
            )

    def test_init_object_with_attribute_that_not_exist(self):
        with self.assertRaises(MissingField):
            Switch(id='sw-01', missing="foo")

    def test_set_object_with_attribute_that_not_exist(self):
        sw_01 = Switch(id='sw-01')
        with self.assertRaises(MissingField):
            sw_01.set(turn_on = True)

    def test_set_attribute_with_wrong_type(self):
        sw_01 = Switch(id='sw-01')
        with self.assertRaises(TypeConflict):
            sw_01.set(status=3)

    
    def test_event_with_wrong_type(self):
        sw_01 = Switch(id='sw-01')
        with self.assertRaises(TypeConflict):
            sw_01.set(status=3)
  

    def test_event_with_set(self):
        sw_01 = Switch(id='sw-01')
        sw_02 = Switch(id='sw-02')
        with self.assertRaises(IllegalEvent):
            Rule(
                Event(sw_01.set(status=True))
            )(
                sw_02.set(status=True)
            )

    def test_rule_with_threshold_condition(self):
        sw_01 = Switch(id='sw-01')
        temp_01 = Thermometer(id='temp-01')
        sw_02 = Switch(id='sw-02')

        sut = Rule(
            Event(sw_01.when(status=True)),
            temp_01.when(temperature='<20')
        )(
            sw_02.set(status=True)
        )

        self.assertEquals(sut.conditions[0].id, 'temp-01')
        self.assertEquals(sut.conditions[0]['temperature'], '<20')

    def test_create_types_with_wrong_field_types(self):
        with self.assertRaises(InvalidFieldType):
            Template('Example', id="wrong_type")

    def test_assign_a_type_to_field_id_different_to_str(self):
        with self.assertRaises(InvalidFieldType) as ex:
            Template('Example', id=int)
        self.assertEquals("id must be str type", str(ex.exception))

    # def test_check_with_wrong_condition(self):
    #     temp_01 = Thermometer(id='temp-01')
    #     with self.assertRaises(InvalidCheck):
    #         Check(temp_01, "bad condition")

    # def test_check_valid_condition_with_blanks(self):
    #     temp_01 = Thermometer(id='temp-01')
    #     sut = Check(temp_01, "temperature < 20")

    #     self.assertEquals(sut['id'], 'temp-01')
    #     self.assertEquals(sut['temperature'], '<20')

    def test_use_value_like_var(self):
        sw_02 = Switch(id='sw-02')
        sw_01 = Switch(id='sw-01')

        sut = Rule(
            Event(sw_02.when(status=True))
        )(
            sw_01.set(status=sw_02.status)
        )

        self.assertTrue(sut.actions[0].status)


    def test_save_any_value_in_var(self):
        sw_02 = Switch(id='sw-02')
        sw_01 = Switch(id='sw-01')

        sut = Rule(
            Event(sw_02.any())
        )(
            sw_01.set(status=sw_02.status)
        )

        self.assertEquals(sut.actions[0].status.deviceId, 'sw-02')
        self.assertEquals(sut.actions[0].status.field, 'status')

    def test_invalid_fact(self):
        temp_01 = Thermometer(id='temp-01')
        with self.assertRaises(InvalidFact):
            Event(temp_01.when(temperature="(AV"))

    def test_statement_with_greater_operator(self):
        temp_01 = Thermometer(id='temp-01')
        sut = Event(temp_01.when(temperature='>20'))
        self.assertEquals(sut.temperature, '>20')
        
    def test_invalid_type_in_str_fact(self):
        sw_01 = Switch(id="sw-01")
        with self.assertRaises(TypeConflict):
            Event(sw_01.when(status="<2"))

    def test_delay(self):
        delay = Delay(30)
        self.assertEquals(delay.time, 30)

    def test_delay_with_bad_value(self):
        with self.assertRaises(InvalidDelayTime) as ex:
            Delay("bad value")
        self.assertEquals("Delay time must be int", str(ex.exception))       
    
    def test_delay_with_negative_value(self):
        with self.assertRaises(InvalidDelayTime) as ex:
            Delay(-4)
        self.assertEquals("Delay time must be a positive number", str(ex.exception))

    # def test_threshold_without_str(self):
    #     sw_01 = Switch(id='sw-01')
    #     temp_01 = Thermometer(id='temp-01')
    #     sw_02 = Switch(id='sw-02')

    #     sut = Rule(
    #         Event(sw_01.when(status=True)),
    #         when(temp_01.temperature < 20)
    #     )(
    #         sw_02.set(status=True)
    #     )

    #     self.assertEquals(sut.conditions[0].id, 'temp-01')
    #     self.assertEquals(sut.conditions[0]['temperature'], '<20')

#     def test_rule_with_variable(self):
#        Rule(
#            AS.f1 << Event(Switch(id='sw-01', status=ANY))
#            AS.f2 << Switch(id='sw-01')
#        )(
#            AS.f2.status << AS.f1.status
#        )
#
#    def test_source_to_deploy(self):
#        sut = SemanticAnalizer("""
#        Rule(
#            ...
#        )(
#            ...
#        )
#        """).parse()
#
#        self.assertTrue(sut.sanity_checks)
#
#        table = sut.get_symbol_table()
#
#        gen = CodeGenerator(table)
#        self.assertEquals(gen.output,
#        """
#           rule = deploy(BoolTemplate, ANY, True))
#           linkTo('ws-01', rule.proxy)
#        """)
#
#        gen.write_to_file('output.py')


class parseArgsTests(TestCase):
    def test_compile(self):
        config = parse_args("--compile placeholder.scn".split())
        self.assertEquals(config.infile, 'placeholder.scn')

    def test_no_args_implies_compile_and_provision(self):
        config = parse_args(["placeholder.scn"])
        self.assertTrue(config.compile)
        self.assertTrue(config.provision)

class ParserTests(TestCase):
    def test_symbol_table_generated(self):
        scene_fd = io.StringIO('''\
Switch = Template('Switch', id=str, status=bool)
switch1 = Switch(id='sw-01', status=True)
switch2 = Switch(id='sw-02', status=True)

Rule(
    Event(switch1.when(status=True)),
)(
    switch2.set(status=True)
)
''')

        sut = Parser(scene_fd)
        sut.run()

        self.assertEquals(
            sut.symbol_table[0].event.id, 'sw-01')
        self.assertTrue(sut.symbol_table[0].event['status'])

        self.assertEquals(sut.symbol_table[0].conditions, [])

        self.assertEquals(
            sut.symbol_table[0].actions[0].id, 'sw-02')
        self.assertTrue(
            sut.symbol_table[0].actions[0]['status'])

    def test_symbol_table_generated_with_event_and_condition(self):
        scene_fd = io.StringIO('''\
Switch = Template('Switch', id=str, status=bool)
Day = Template('Day', id=str, status=bool)

sw_01 = Switch(id='sw-01')
sw_02 = Switch(id='sw-02')
day = Day(id='day')

Rule(
    Event(sw_01.when(status=True)),
    day.when(status=False)
)(
    sw_02.set(status=True)
)
''')
        sut = Parser(scene_fd)
        sut.run()

        self.assertEquals(
            sut.symbol_table[0].event.id, 'sw-01')
        self.assertTrue(sut.symbol_table[0].event['status'])

        self.assertEquals(
            sut.symbol_table[0].conditions[0].id, 'day')
        self.assertFalse(sut.symbol_table[0].conditions[0]['status'])

        self.assertEquals(
            sut.symbol_table[0].actions[0].id,'sw-02')
        self.assertTrue(sut.symbol_table[0].actions[0]['status'])

    def test_symbol_table_generated_with_threshold(self):
        scene_fd = io.StringIO('''\
Switch = Template('Switch', id=str, status=bool)
Thermometer = Template('Thermometer', id=str, temperature=int)

sw_01 = Switch(id='sw-01')
sw_02 = Switch(id='sw-02')
temp_01 = Thermometer(id='temp-01')

Rule(
    Event(sw_01.when(status=True)),
    temp_01.when(temperature='<20')
)(
    sw_02.set(status=True)
)
''')
        sut = Parser(scene_fd)
        sut.run()
        self.assertEquals(
            sut.symbol_table[0].event.id, 'sw-01')
        self.assertTrue(sut.symbol_table[0].event['status'])
        
        self.assertEquals(
            sut.symbol_table[0].conditions[0].id, 'temp-01')
        self.assertEquals(sut.symbol_table[0].conditions[0]['temperature'], '<20')
        
        self.assertEquals(
            sut.symbol_table[0].actions[0].id,'sw-02')
        self.assertTrue(sut.symbol_table[0].actions[0]['status'])

    def test_rule_with_two_actions(self):
        scene_fd = io.StringIO('''\
Switch = Template('Switch', id=str, status=bool)
Thermometer = Template('Thermometer', id=str, temperature=int)

sw_01 = Switch(id='sw-01')
sw_02 = Switch(id='sw-02')
temp_01 = Thermometer(id='temp-01')

Rule(
    Event(sw_01.when(status=True)),
    temp_01.when(temperature='<20')
)(
    sw_02.set(status=True),
    sw_01.set(status=False)
)
''')
        sut = Parser(scene_fd)
        sut.run()
        self.assertTrue(sut.symbol_table[0].actions[0].status)
        self.assertFalse(sut.symbol_table[0].actions[1].status)

    def test_rule_with_delay(self):
        scene_fd = io.StringIO('''\
Switch = Template('Switch', id=str, status=bool)
Thermometer = Template('Thermometer', id=str, temperature=int)

sw_01 = Switch(id='sw-01')
sw_02 = Switch(id='sw-02')
temp_01 = Thermometer(id='temp-01')

Rule(
    Event(sw_01.when(status=True)),
    temp_01.when(temperature='<20')
)(
    sw_02.set(status=True),
    Delay(20),
    sw_01.set(status=False)
)
''')
        sut = Parser(scene_fd)
        sut.run()
        self.assertTrue(sut.symbol_table[0].actions[0].status)
        self.assertFalse(sut.symbol_table[0].actions[2].status)
        self.assertEquals(sut.symbol_table[0].actions[1].time, 20)

                