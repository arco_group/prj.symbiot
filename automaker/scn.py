#!/usr/bin/python3

import os
import collections
import argparse
import sys
import Ice
import time
import re
import random
import json

slice_path = "automaker.ice"

if not os.path.exists(slice_path):
    slice_path = "../../../automaker.ice"

Ice.loadSlice("/usr/share/slice/st/st.ice")
Ice.loadSlice("/usr/share/ice/slice/SmartStuff/wiring.ice -I/usr/share/slice --all")
Ice.loadSlice('/usr/share/ice/slice/IceCloud/ServerFactory.ice')
Ice.loadSlice('/usr/share/ice/slice/PropertyService/PropertyService.ice')
Ice.loadSlice("-I/usr/share/ice/slice {} --all".format(slice_path))


import IceCloud
import SmartStuff
import st
import PropertyService
import Automaker


class Engine:
    rules = []

    @classmethod
    def reset(cls):
        cls.rules = []


class Rule:
    def __init__(self, *conditions):
        Engine.rules.append(self)
        self.conditions, self.event = self.check_conditions(conditions)
        self.actions = []

    def check_conditions(self, conditions):
        event = None
        conditions = list(conditions)
        
        for c in conditions:
            if not isinstance(c, Condition) and not isinstance(c, Event):
                raise InvalidCondition(c)
            if isinstance(c, Event):
                event = c
                conditions.remove(c)

        if event is None:
            raise EventNotFound

        return conditions, event

    def __call__(self, *actions):
        self.actions = self.check_actions(actions)
        return self

    def check_actions(self, actions):
        for a in actions:
            if not isinstance(a, Action):
                raise InvalidAction

        return list(actions)


def when(condition):
    return condition


class Statement(dict):
    field_spec = {}

    def __init__(self, **kwargs):
        if 'id' not in kwargs.keys():
            raise MissingField('id')

        self.update(kwargs)

    def when(self, **kwargs):
        updated = dict()
        updated['id'] = self['id']
        updated.update(kwargs)
        condition_class = type('Condition-{}'.format(random.randint(1,1000)), (self.__class__, Condition), {})
        return condition_class(**updated)

    def set(self, **kwargs):
        updated = dict()
        updated['id'] = self['id']
        updated.update(kwargs)
        action_class = type('Action-{}'.format(random.randint(1,1000)), (self.__class__, Action), {})
        return action_class(**updated)

    def any(self):
        return self

    def update(self, new_fields):
        for name, value in new_fields.items():
            if name not in self.field_spec:
                raise MissingField(name)

            if isinstance(value, str) and self.field_spec[name] != str:
                self.check_str(value)
                value = eval(value[1:])

            if not isinstance(value, (self.field_spec[name], str, Automaker.Copy)) :
                raise TypeConflict("filed '{}' type should be '{}', but value was '{}'".format(
                    name, self.field_spec[name].__name__, value))

        super().update(new_fields)
    
    def __getattr__(self, key):
        return self[key]

    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            return Automaker.Copy(self.id, key)

    def check_str(self, fact):
        if not re.match(r"[<>][0-9]+", fact):
            raise InvalidFact


class InvalidFact(Exception):
    pass


class Action:
    pass


class Condition:
    pass


class InvalidCondition(Exception):
    pass


class InvalidAction(Exception):
    pass


class EventNotFound(Exception):
    pass


class Event:
    def __new__(cls, condition):
        event_class = type('Event-{}'.format(random.randint(1, 1000)), (Event, condition.__class__), {})
        return condition.__new__(event_class)

    def __init__(self, condition):
        if isinstance(condition, Statement) and isinstance(condition, Action):
            raise IllegalEvent

        super().__init__(**condition)

class Delay(Action):
    def __init__(self, time):
        self.check_time(time)
        self.time = time

    def check_time(self, time):
        if not isinstance(time, int):
            raise InvalidDelayTime("Delay time must be int")
        elif time < 0:
            raise InvalidDelayTime("Delay time must be a positive number")

class IllegalEvent(Exception):
    pass


class InvalidDelayTime(Exception):
    pass


class Template(type):
    def __new__(cls, class_name, **fields):
        if 'id' not in fields:
            raise MissingField("id")
        
        for value in fields.values():
            if not isinstance(value, type):
                raise InvalidFieldType

        if fields['id'] is not str:
            raise InvalidFieldType("id must be str type")

        retval = type(class_name, (Statement,), {})
        # retval.field_spec.update(fields)
        retval.field_spec = fields
        return retval


class InvalidFieldType(Exception):
    pass


class MissingField(Exception):
    pass


class TypeConflict(Exception):
    pass

# # FIXME: Deprecated, in future works remove # #
class VariableContainer(dict):
    def __lshift__(self, object_to_save):
        self.update(object_to_save)
        return object_to_save

    def __getattr__(self, key):
        return self[key]
    
    def __getitem__(self, key):
        try:
            return super().__getitem__(key)
        except KeyError:
            return Automaker.Copy(self.id, key)


class Var:
    def __getattr__(self, name):
        self.__dict__[name] = VariableContainer()
        return self.__dict__[name]

    def clean(self):
        self.__dict__.clear()


var = Var()

# # FIXME: Check not working, remove? # # 
def Check(device, condition):
    condition = condition.replace(' ', '')
    if '<' in condition:
        cond_partitioned = condition.partition('<')
    elif '>' in condition:
        cond_partitioned = condition.partition('>')
    else:
        raise InvalidCheck

    Condition.field_spec = device.field_spec

    return Condition(id=device['id'],
                    **{cond_partitioned[0]: cond_partitioned[1] + cond_partitioned[2]})


class InvalidCheck(Exception):
    pass


def parse_args(args):
        parser = argparse.ArgumentParser(description='Automaker compiler and provisioner.')

        parser.add_argument('-c', '--compile', action='store_true',
                            help='generates provision script from automation scene.')

        parser.add_argument('-p', '--provision', action='store_true',
                            help='performs scene provision.')

        parser.add_argument("infile", help="source code")

        retval = parser.parse_args(args)

        if not retval.compile and not retval.provision:
            retval.compile = retval.provision = True

        retval.scene_filename = None
        retval.provision_filename = None

        if retval.compile:
            retval.scene_filename = retval.infile
            retval.provision_filename = os.path.splitext(retval.scene_filename)[0] + '.prv' 

        if retval.provision:
            if retval.provision_filename is None:
                retval.provision_filename = retval.infile

        return retval


class Parser:
    def __init__(self, fd):
        self.fd = fd
        self.symbol_table = []
        self.scene = fd.read()

    @classmethod
    def from_filename(self, scene_filename):
        with open(scene_filename, 'rt') as fd:
            return Parser(fd)

    def run(self):
        Engine.reset()
        exec(self.scene)
        self.symbol_table = Engine.rules
        return self.symbol_table


class SemanticAnalyzer:
    def __init__(self, symbol_table):
        pass


class CodeGenerator:
    def __init__(self, symbol_table, file_name):
        self.create_templates = {
            bool: "\nop = createOperator('BoolOperatorTemplate', 'Bool{}Rule{}Op{}')\n",
            int: "\nop = createOperator('FloatOperatorTemplate', 'Float{}Rule{}Op{}')\n"
        }
        self.configure_with_action = "configureOperator(op, when='{}', met={}, then='{}', delay={})\n"
        self.configure_with_var = "configureOperatorWithVar(op, when='{}', met={}, then={}, delay={})\n"
        self.link_line = "linkServices(str(op), source='{}', sink='{}')\n"

        self.symbol_table = self.check_rules(symbol_table)
        self.output = []
        self.file_name = os.path.splitext(os.path.basename(file_name))[0]

    def check_rules(self, symbol_table):
        if symbol_table == []:
            raise InvalidSymbolTable()
        for r in symbol_table:
            if not isinstance(r, Rule):
                raise InvalidSymbolTable()

        return symbol_table

    def build(self):
        rule_number = 1
        for r in self.symbol_table:
            self.output.extend(self.build_rule(r, rule_number))
            rule_number += 1

    def write_to_fd(self, fd):
        self.output = '\n'.join(self.output)
        fd.write(self.output)
        fd.flush()

    def write_to_filename(self, filename):
        with open(filename, 'wt') as fd:
            self.write_to_fd(fd)

    def build_rule(self, rule, rule_number):
        source_id = rule.event.pop('id', None)
        try:
            event_value = list(rule.event.values())[0]
        except IndexError:
            event_value = ''

        if event_value == '':
            event_type = bool
        elif isinstance(event_value, str):
            event_type = type(evaluate(event_value))
        else:
            event_type = type(event_value)
             
        conditions = self.build_conditions(rule.conditions)

        code = []
        operator_number = 1
        delay = 0
        for action in rule.actions:
            if isinstance(action, Delay):
                delay += action.time
                continue

            sink_id = action.pop('id', None)
            action_value = action.pop('status', None)

            create_line = self.create_templates[event_type].format(
                self.file_name, rule_number, operator_number)
            operator_number += 1

            if isinstance(action_value, Automaker.Copy):
                action_value = "Copy('{}', '{}')".format(
                    action_value.deviceId, action_value.field)
                configure_line = self.configure_with_var.format(
                    event_value, conditions, action_value, delay)
            else:
                configure_line = self.configure_with_action.format(
                    event_value, conditions, action_value, delay)

            code.append(create_line + configure_line + self.link_line.format(source_id, sink_id))
            
        return code

    def build_conditions(self, conditions):
        retval = {}
        for c in conditions:
            retval[c.id] = []
            for attr, value in c.items():
                if attr != 'id':
                    retval[c.id].append((attr, str(value)))
        return retval

def evaluate(string_to_eval):
    try:
        return eval(string_to_eval[1:])
    except Exception:
        return str


class InvalidSymbolTable(Exception):
    pass


class Provisioner:
    def __init__(self, factory, wiring, property_server, provision_filename, communicator, resolver):
        self.factory = factory
        self.wiring = wiring
        self.property_server = property_server
        self.provision_filename = provision_filename
        self.ic = communicator
        self.resolver = resolver

    @classmethod
    def from_str_proxies(self, provision_filename, communicator, resolver):
        def retry_cast(str_proxy, cast):
            proxy = communicator.stringToProxy(str_proxy)
            for i in range(5):
                try:
                    retval = cast.checkedCast(proxy)
                    break
                except Ice.ObjectNotExistException:
                    print('.', end='')
                    time.sleep(0.5)
                else:
                    raise Exception("Object '{}' not available".format(proxy))

            if not retval:
                raise RuntimeError("Invalid proxy: '{}'".format(proxy))

            return retval

        return Provisioner(retry_cast('factory', IceCloud.ServerFactoryPrx),
                           retry_cast('Wirer', SmartStuff.WirerPrx),
                           retry_cast('PropertyServer', PropertyService.PropertyServerPrx),
                           provision_filename, communicator, resolver)

    def run(self):
        with open(self.provision_filename) as fd:
            exec(fd.read(), dict(createOperator=self.createOperator, configureOperator=self.configureOperator, 
                                    configureOperatorWithVar=self.configureOperatorWithVar, linkServices=self.linkServices,
                                    Copy=Automaker.Copy))

    def createOperator(self, template, name):
        node = self.ic.getProperties().getProperty('node')

        operator_proxy = self.factory.make(node, template, {'name': name})
        operator = Automaker.OperatorPrx.checkedCast(operator_proxy)
        
        return operator

    def configureOperator(self, operator, when, met, then, delay):
        operator.setEvent(when)
        operator.setAction(then)
        operator.setConditions(met)
        operator.setDelay(delay)
        operator.commit()

    def configureOperatorWithVar(self, operator, when, met, then, delay):
        operator.setEvent(when)
        operator.setActionWithVar(then)
        operator.setConditions(met)
        operator.setDelay(delay)
        operator.commit()

    def linkServices(self, operator_string_proxy, source, sink):
        source_string_proxy = self.resolver.resolve(source, self.property_server)
        self.wiring.addObserver(source_string_proxy, operator_string_proxy)
        sink_string_proxy = self.resolver.resolve(sink, self.property_server)
        self.wiring.addObserver(operator_string_proxy, sink_string_proxy)


class Resolver:
    def __init__(self, communicator):
        self.ic = communicator

    def resolve(self, identity, property_server):
        return json.loads(property_server.get("{} | proxy".format(identity)))

def main(argv, communicator=None):
    config = parse_args(argv[1:])

    if config.compile:
        symbol_table = Parser.from_filename(config.scene_filename).run()

        generator = CodeGenerator(symbol_table, config.scene_filename)
        generator.build()
        generator.write_to_filename(config.provision_filename)

    if config.provision:
        provisioner = Provisioner.from_str_proxies(
            config.provision_filename, communicator, Resolver(communicator))
        provisioner.run()


class Compiler(Ice.Application):
    def run(self, args):
        main(args, self.communicator())


if __name__ == '__main__':
    sys.exit(Compiler().main(sys.argv))
