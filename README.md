# SymbIoT

SymbIoT propone la investigación, desarrollo e implementación de nuevas técnicas para:


1. La composición y reconfiguración automática de servicios dará respuesta a las necesidades de actuación emergentes. Para ello, diseñaremos una plataforma de razonamiento distribuido (DRP) que posibilitará la interpretación de lo que ocurre en el entorno y la articulación de respuestas automáticas. Será necesario establecer un vocabulario común, con su correspondiente semántica (ontología), y un conjunto bien definido de interfaces de programación asociadas que simplificarán el proceso de desarrollo, despliegue e integración de servicios para entornos IoT
2. Ofrecer un protocolo de red virtual que dé soporte al intercambio de mensajes entre dominios heterogéneos, protocolo que hemos denominado Inter-Domain Messaging (IDM), abordará el problema de la heterogeneidad en las tecnologías de red subyacentes. 
3. El uso de las técnicas de virtualidad aumentada y el paradigma de los gemelos virtuales permitirán el control, visualización y simulación del ecosistema IoT con funcionalidades avanzadas (extendiendo la idea de los tradicionales SCADA). Estas técnicas serán especialmente útiles para la integración del factor humano para la simulación de escenarios que entrañen complejidad o riesgo para las personas.	
4. A nivel de procesamiento, las soluciones hardware basadas en la lógica reconfigurable podrán aportar contención en el consumo, gestión dinámica de los recursos, seguridad, flexibilidad y adaptabilidad.
	
SymbIoT, en su fase de validación, se aplicará el ámbito de los edificios inteligentes o Smart Buildings, siendo éste un subdomino del paradigma de ciudad inteligente o Smart City. Estos entornos pretenden mejorar la calidad de vida de las personas a la vez que se satisfacen objetivos de uso eficiente de recursos y energía. Como casos de uso principales para este entorno se considerarán la mejora del confort, bienestar y seguridad de las personas presentes en un edificio junto con la optimización del consumo energético del mismo. 
